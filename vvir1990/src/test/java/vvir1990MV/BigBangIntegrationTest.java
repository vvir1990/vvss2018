package vvir1990MV;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import vvir1990MV.controller.BibliotecaController;
import vvir1990MV.model.Carte;
import vvir1990MV.repository.repoInterfaces.ICartiRepository;
import vvir1990MV.repository.repoMock.CartiRepositoryMock;

import java.util.List;

public class BigBangIntegrationTest {

    private ICartiRepository repository;
    private BibliotecaController bibliotecaController;
    private Carte carte;
    private Carte carteComparatie;

    @Before
    public void setUp() {
        repository = new CartiRepositoryMock();
        bibliotecaController = new BibliotecaController(repository);
        carte = new Carte();
        carte.setIsbn("1ABC");
        carte.setTitlu("Cartea mea");
        carte.adaugaAutor("Victor Variu");
        carte.adaugaAutor("Anonimul Anonim");
        carte.setAnAparitie(2018);
        carte.setEditura("in-house");
        carte.adaugaCuvantCheie("cartea");
        carte.adaugaCuvantCheie("mea");
        carte.adaugaCuvantCheie("cartea mea");
        carteComparatie = new Carte();
        carteComparatie.setIsbn("ISBN");
        carteComparatie.setTitlu("Titlu");
        carteComparatie.adaugaAutor("Autor Autor");
        carteComparatie.setAnAparitie(2018);
        carteComparatie.setEditura("Editura");
        carteComparatie.adaugaCuvantCheie("cuvant");
    }

    @Test
    public void testUnitA() {
        int size = bibliotecaController.getCarti().size();

        carte.setAnAparitie(Integer.MAX_VALUE - 1);

        try {
            bibliotecaController.adaugaCarte(carte);
        } catch (Exception e) {
            Assert.fail();
        }
        Assert.assertEquals(size + 1, bibliotecaController.getCarti().size());
    }

    @Test
    public void testUnitB() {
        try {
            bibliotecaController.adaugaCarte(carte);
            bibliotecaController.adaugaCarte(carteComparatie);
        } catch (Exception e) {
            Assert.fail();
        }
        List<Carte> carti = bibliotecaController.getCartiOrdonateDinAnul(2018);
        Assert.assertEquals(carte, carti.get(0));
        Assert.assertEquals(carteComparatie, carti.get(1));
    }

    @Test
    public void testUnitC() {
        repository.adaugaCarte(carte);
        try {
            Assert.assertEquals(1, bibliotecaController.cautaCarte("Victor").size());
            Assert.assertEquals(carte, bibliotecaController.cautaCarte("Victor").get(0));
        } catch (Exception e) {
            Assert.fail();
        }
    }

    @Test
    public void testIntegrationAll() {
        repository.adaugaCarte(carteComparatie);
        Carte carteMock = new Carte();
        carteMock.setIsbn("a");
        carteMock.setAnAparitie(1);
        carteMock.adaugaAutor("a");
        carteMock.adaugaCuvantCheie("a");
        carteMock.setTitlu("a");
        carteMock.setEditura("a");
        repository.adaugaCarte(carteMock);
        try {
            int size = bibliotecaController.getCarti().size();
            bibliotecaController.adaugaCarte(carte);
            Assert.assertEquals(size + 1, bibliotecaController.getCarti().size());
            Assert.assertTrue(bibliotecaController.getCarti().contains(carte));
            Assert.assertEquals(carte, bibliotecaController.getCarti().get(size));
        } catch (Exception e) {
            Assert.fail();
        }
        List<Carte> carti2018 = bibliotecaController.getCartiOrdonateDinAnul(2018);
        Assert.assertEquals(2, carti2018.size());
        Assert.assertEquals(carte, carti2018.get(0));
        Assert.assertEquals(carteComparatie, carti2018.get(1));
        try {
            Assert.assertEquals(1, bibliotecaController.cautaCarte("Victor").size());
            Assert.assertEquals(carte, bibliotecaController.cautaCarte("Victor").get(0));
        } catch (Exception e) {
            Assert.fail();
        }
    }
}
