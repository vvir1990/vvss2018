package vvir1990MV;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import vvir1990MV.controller.BibliotecaController;
import vvir1990MV.model.Carte;
import vvir1990MV.repository.repoInterfaces.ICartiRepository;
import vvir1990MV.repository.repoMock.CartiRepositoryMock;

import java.util.ArrayList;
import java.util.List;

public class BibliotecaControllerBBTTest {

    private ICartiRepository repository;
    private BibliotecaController bibliotecaController;
    private Carte carte;

    @Before
    public void setUp() {
        repository = new CartiRepositoryMock();
        bibliotecaController = new BibliotecaController(repository);
        carte = new Carte();
        carte.setIsbn("1ABC");
        carte.setTitlu("Cartea mea");
        carte.adaugaAutor("Victor Variu");
        carte.adaugaAutor("Anonimul Anonim");
        carte.setAnAparitie(2018);
        carte.setEditura("in-house");
        carte.adaugaCuvantCheie("cartea");
        carte.adaugaCuvantCheie("mea");
        carte.adaugaCuvantCheie("cartea mea");
    }

    @Test
    public void testAdaugaCarte1() {
        carte.setTitlu(null);
        try {
            bibliotecaController.adaugaCarte(carte);
            Assert.fail();
        } catch (Exception e) {
            Assert.assertEquals(e.getClass(), NullPointerException.class);
        }
    }

    @Test
    public void testAdaugaCarte2() {
        carte.setTitlu("");
        try {
            bibliotecaController.adaugaCarte(carte);
            Assert.fail();
        } catch (Exception e) {
            Assert.assertEquals(e.getMessage(), "Titlu invalid!");
        }
    }

    @Test
    public void testAdaugaCarte3() {
        carte.setAutori(null);
        try {
            bibliotecaController.adaugaCarte(carte);
            Assert.fail();
        } catch (Exception e) {
            Assert.assertEquals(e.getMessage(), "Lista autori vida!");
        }
    }

    @Test
    public void testAdaugaCarte4() {
        carte.setAutori(new ArrayList<String>());
        carte.adaugaAutor("Victor Variu");
        carte.adaugaAutor("");
        try {
            bibliotecaController.adaugaCarte(carte);
            Assert.fail();
        } catch (Exception e) {
            Assert.assertEquals(e.getMessage(), "Autor invalid!");
        }
    }

    @Test
    public void testAdaugaCarte5() {
        carte.setAnAparitie(null);
        try {
            bibliotecaController.adaugaCarte(carte);
            Assert.fail();
        } catch (Exception e) {
            Assert.assertEquals(e.getClass(), NullPointerException.class);
        }
    }

    @Test
    public void testAdaugaCarte6() {
        int size = bibliotecaController.getCarti().size();

        carte.setAutori(new ArrayList<String>());

        try {
            bibliotecaController.adaugaCarte(carte);
        } catch (Exception e) {
            Assert.fail();
        }
        Assert.assertEquals(size + 1, bibliotecaController.getCarti().size());
    }

    @Test
    public void testAdaugaCarte7() {
        int size = bibliotecaController.getCarti().size();

        carte.setAnAparitie(Integer.MIN_VALUE);

        try {
            bibliotecaController.adaugaCarte(carte);
        } catch (Exception e) {
            Assert.fail();
        }
        Assert.assertEquals(size + 1, bibliotecaController.getCarti().size());
    }

    @Test
    public void testAdaugaCarte8() {
        int size = bibliotecaController.getCarti().size();

        carte.setAnAparitie(Integer.MAX_VALUE - 1);

        try {
            bibliotecaController.adaugaCarte(carte);
        } catch (Exception e) {
            Assert.fail();
        }
        Assert.assertEquals(size + 1, bibliotecaController.getCarti().size());
    }

    @Test
    public void testAdaugaCarte9() {
        int size = bibliotecaController.getCarti().size();

        String maxLengthString = "7777777";
        for (int i = 0; i < 31; i++) {
            maxLengthString = maxLengthString.concat("88888888");
        }
        carte.setEditura(maxLengthString);
        Assert.assertEquals(255, maxLengthString.length());

        carte.setEditura(maxLengthString);

        try {
            bibliotecaController.adaugaCarte(carte);
        } catch (Exception e) {
            Assert.fail();
        }
        Assert.assertEquals(size + 1, bibliotecaController.getCarti().size());
    }

    @Test
    public void testAdaugaCarte10() {
        int size = bibliotecaController.getCarti().size();
        try {
            bibliotecaController.adaugaCarte(carte);
        } catch (Exception e) {
            Assert.fail();
        }
        Assert.assertEquals(size + 1, bibliotecaController.getCarti().size());
    }

    @Test
    public void testCautaCarteNevalid() {
        repository.adaugaCarte(carte);

        try {
            bibliotecaController.cautaCarte("ana2");
            Assert.fail();
        } catch (Exception e) {
            Assert.assertEquals(BibliotecaController.NUME_INCORECT_AUTOR, e.getMessage());
        }
    }

    @Test
    public void testCautaCarteValid() {
        repository.adaugaCarte(carte);

        try {
            List<Carte> carti = bibliotecaController.cautaCarte("Victor");
            Assert.assertEquals(1, carti.size());
            Assert.assertEquals(carte, carti.get(0));
        } catch (Exception e) {
            Assert.fail();
        }
    }
}
