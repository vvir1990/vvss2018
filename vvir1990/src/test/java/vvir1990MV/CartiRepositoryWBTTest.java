package vvir1990MV;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import vvir1990MV.model.Carte;
import vvir1990MV.repository.repoMock.CartiRepositoryMock;

public class CartiRepositoryWBTTest {
    private CartiRepositoryMock repository;
    private Carte carte1;
    private Carte carte2;

    @Before
    public void setUp() {
        repository = new CartiRepositoryMock();
        carte1 = new Carte();
        carte1.setIsbn("isbn");
        carte1.setTitlu("titlu");
        carte1.adaugaAutor("autor1");
        carte1.setAnAparitie(1);
        carte1.setEditura("editura");
        carte1.adaugaCuvantCheie("cuvant1");
        carte2 = new Carte();
        carte2.setIsbn("isbn");
        carte2.setTitlu("titlu");
        carte2.adaugaAutor("autor1");
        carte2.setAnAparitie(1);
        carte2.setEditura("editura");
        carte2.adaugaCuvantCheie("cuvant1");
    }

    @Test
    public void testComparaCarti1() {
        carte2.setTitlu("title");
        Assert.assertTrue(repository.comparaCarti(carte1, carte2) > 0);
    }

    @Test
    public void testComparaCarti2() {
        carte1.deleteTotiAutorii();
        carte2.deleteTotiAutorii();
        Assert.assertTrue(repository.comparaCarti(carte1, carte2) < 0);
    }

    @Test
    public void testComparaCarti3() {
        carte2.deleteTotiAutorii();
        Assert.assertTrue(repository.comparaCarti(carte1, carte2) > 0);
    }

    @Test
    public void testComparaCarti4() {
        Assert.assertTrue(repository.comparaCarti(carte1, carte2) < 0);
    }

    @Test
    public void testComparaCarti5() {
        carte2.deleteTotiAutorii();
        carte2.adaugaAutor("autor");
        Assert.assertTrue(repository.comparaCarti(carte1, carte2) > 0);
    }

    @Test
    public void testComparaCarti6() {
        carte1.adaugaAutor("autor2");
        Assert.assertTrue(repository.comparaCarti(carte1, carte2) > 0);
    }
}
