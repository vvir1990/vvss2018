package vvir1990MV.view;


import vvir1990MV.controller.BibliotecaController;
import vvir1990MV.model.Carte;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class ConsolaUI {

    private BufferedReader console;
    private BibliotecaController bibliotecaController;

    public ConsolaUI(BibliotecaController bibliotecaController) {
        this.bibliotecaController = bibliotecaController;
    }

    public void executa() throws IOException {
        console = new BufferedReader(new InputStreamReader(System.in));

        int opt = Integer.MIN_VALUE;
        while (opt != 0) {

            printMenu();
            String line;
            do {
                System.out.println("Introduceti o optiune valida: ");
                line = console.readLine();
            } while (!line.matches("[0-4]"));
            opt = Integer.parseInt(line);

            switch (opt) {
                case 1:
                    adaugaCarte();
                    break;
                case 2:
                    cautaCartiDupaAutor();
                    break;
                case 3:
                    afiseazaCartiOrdonateDinAnul();
                    break;
                case 4:
                    afiseazaToateCartile();
                    break;
            }
        }
    }

    private void printMenu() {
        System.out.println("\n\n\n");
        System.out.println("Evidenta cartilor dintr-o biblioteca");
        System.out.println("     1. Adaugarea unei noi carti");
        System.out.println("     2. Cautarea cartilor scrise de un anumit autor");
        System.out.println("     3. Afisarea cartilor din biblioteca care au aparut intr-un anumit an, ordonate alfabetic dupa titlu si autori");
        System.out.println("     4. Afisarea toturor cartilor");
        System.out.println("     0. Exit");
    }

    private void adaugaCarte() {
        Carte carte = new Carte();
        try {
            System.out.println("\n\n\n");

            System.out.println("ISBN: ");
            carte.setIsbn(console.readLine());

            System.out.println("Titlu: ");
            carte.setTitlu(console.readLine());

            System.out.println("Editura: ");
            carte.setEditura(console.readLine());

            String line;
            do {
                System.out.println("An aparitie: ");
                line = console.readLine();
            } while (!line.matches("[0-9]|[1-9][0-9]*"));
            carte.setAnAparitie(Integer.parseInt(line));

            do {
                System.out.println("Nr. de autori: ");
                line = console.readLine();
            } while (!line.matches("[1-9][0-9]*"));
            int nrAutori = Integer.parseInt(line);
            for (int i = 1; i <= nrAutori; i++) {
                System.out.println("Autor " + i + ": ");
                carte.adaugaAutor(console.readLine());
            }

            do {
                System.out.println("Nr. de cuvinte cheie:");
                line = console.readLine();
            } while (!line.matches("[1-9][0-9]*"));
            int nrCuvinte = Integer.parseInt(line);
            for (int i = 1; i <= nrCuvinte; i++) {
                System.out.println("Cuvant " + i + ": ");
                carte.adaugaCuvantCheie(console.readLine());
            }

            bibliotecaController.adaugaCarte(carte);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    private void afiseazaToateCartile() {
        System.out.println("\n\n\n");
        for (Carte carte : bibliotecaController.getCarti())
            System.out.println(carte);
    }

    private void cautaCartiDupaAutor() {
        System.out.println("\n\n\n");
        System.out.println("Autor:");
        try {
            for (Carte c : bibliotecaController.cautaCarte(console.readLine())) {
                System.out.println(c);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void afiseazaCartiOrdonateDinAnul() {
        System.out.println("\n\n\n");
        try {
            String line;
            do {
                System.out.println("An aparitie:");
                line = console.readLine();
            } while (!line.matches("[10-9]+"));
            for (Carte c : bibliotecaController.getCartiOrdonateDinAnul(Integer.parseInt(line))) {
                System.out.println(c);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
