package vvir1990MV.controller;


import vvir1990MV.model.Carte;
import vvir1990MV.repository.repoInterfaces.ICartiRepository;
import vvir1990MV.util.ValidatorCarte;

import java.util.List;

public class BibliotecaController {

    public static final String NUME_INCORECT_AUTOR = "Nume incorect pentru autor.";

    private ICartiRepository cartiRepository;

    public BibliotecaController(ICartiRepository cartiRepository) {
        this.cartiRepository = cartiRepository;
    }

    public void adaugaCarte(Carte carte) throws Exception {
        ValidatorCarte.validateCarte(carte);
        cartiRepository.adaugaCarte(carte);
    }

    public void modificaCarte(Carte nou, Carte vechi) {
        cartiRepository.modificaCarte(nou, vechi);
    }

    public void stergeCarte(Carte carte) {
        cartiRepository.stergeCarte(carte);
    }

    public List<Carte> cautaCarte(String autor) throws Exception {
        if(!ValidatorCarte.isAlpha(autor)) {
            throw new Exception(NUME_INCORECT_AUTOR);
        }
        return cartiRepository.cautaCarte(autor);
    }

    public List<Carte> getCarti() {
        return cartiRepository.getCarti();
    }

    public List<Carte> getCartiOrdonateDinAnul(Integer an) {
        return cartiRepository.getCartiOrdonateDinAnul(an);
    }
}
