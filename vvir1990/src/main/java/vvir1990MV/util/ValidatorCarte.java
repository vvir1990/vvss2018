package vvir1990MV.util;

import vvir1990MV.model.Carte;

public class ValidatorCarte {

    public static void validateCarte(Carte c) throws Exception {
        if (c.getCuvinteCheie() == null) {
            throw new Exception("Lista cuvinte cheie vida!");
        }
        if (c.getAutori() == null) {
            throw new Exception("Lista autori vida!");
        }
        if (isNumber(c.getAnAparitie().toString()));
        if (!validateInputString(c.getTitlu()))
            throw new Exception("Titlu invalid!");
        for (String s : c.getAutori()) {
            if (!validateInputString(s))
                throw new Exception("Autor invalid!");
        }
        for (String s : c.getCuvinteCheie()) {
            if (!validateInputString(s))
                throw new Exception("Cuvant cheie invalid!");
        }
    }

    public static boolean isAlpha(String s) {
        return s.matches("[a-zA-Z]+");
    }

    public static boolean isNumber(String s) {
        return s.matches("[0-9]+");
    }

    private static boolean validateInputString(String string) {
        String regex = "[a-zA-Z]+";
        String[] t = string.split(" ");
        if (t.length == 2) {
            boolean ok1 = t[0].matches(regex);
            boolean ok2 = t[1].matches(regex);
            return ok1 && ok2;
        }
        return isAlpha(string);
    }
}
