package vvir1990MV.repository.repoMock;


import vvir1990MV.model.Carte;
import vvir1990MV.repository.repoInterfaces.ICartiRepository;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class CartiRepositoryMock implements ICartiRepository {

    protected List<Carte> carti = new ArrayList<Carte>();

    public CartiRepositoryMock() {
        carti.add(Carte.getCarteFromString("1ABC;Povesti;Mihai Eminescu,Ion Caragiale,Ion Creanga;1973;Corint;povesti,povestiri"));
        carti.add(Carte.getCarteFromString("2DEF;Poezii;Sadoveanu;1973;Corint;poezii"));
        carti.add(Carte.getCarteFromString("3GHI;Enigma Otiliei;George Calinescu;1948;Litera;enigma,otilia"));
        carti.add(Carte.getCarteFromString("4JKL;Dale carnavalului;Caragiale Ion;1948;Litera;caragiale,carnaval"));
        carti.add(Carte.getCarteFromString("5MNO;Intampinarea crailor;Mateiu Caragiale;1948;Litera;mateiu,crailor"));
        carti.add(Carte.getCarteFromString("6PQR;Test;Calinescu,Tetica;1992;Pipa;am,casa"));
    }

    public void adaugaCarte(Carte carte) {
        carti.add(carte);
    }

    public List<Carte> cautaCarte(String numeAutor) {
        List<Carte> carti = getCarti();
        List<Carte> cartiGasite = new ArrayList<Carte>();
        for (Carte carte : carti) {
            boolean gasit = false;
            List<String> autori = carte.getAutori();
            for (String autor : autori) {
                if (autor.contains(numeAutor)) {
                    gasit = true;
                    break;
                }
            }
            if (gasit) {
                cartiGasite.add(carte);
            }
        }

        return cartiGasite;
    }

    public List<Carte> getCarti() {
        return carti;
    }

    public void modificaCarte(Carte nou, Carte vechi) {
        // TODO Auto-generated method stub

    }

    public void stergeCarte(Carte carte) {
        // TODO Auto-generated method stub

    }

    public List<Carte> getCartiOrdonateDinAnul(Integer an) {
        List<Carte> carti = getCarti();
        List<Carte> rezultat = new ArrayList<Carte>();
        for (Carte carte : carti) {
            if (carte.getAnAparitie().equals(an)) {
                rezultat.add(carte);
            }
        }

        Collections.sort(rezultat, new Comparator<Carte>() {
            public int compare(Carte a, Carte b) {
                return comparaCarti(a, b);
            }

        });

        return rezultat;
    }

    public int comparaCarti(Carte a, Carte b) {
        if (a.getTitlu().equals(b.getTitlu())) {
            int i = 0;
            while (i < a.getAutori().size() && i < b.getAutori().size()) {
                if (a.getAutori().get(i).equals(b.getAutori().get(i))) {
                    i++;
                } else {
                    return a.getAutori().get(i).compareTo(b.getAutori().get(i));
                }
            }
            if (i == a.getAutori().size()) {
                return -1;
            }
            if (i == b.getAutori().size()) {
                return 1;
            }
        }
        return a.getTitlu().compareTo(b.getTitlu());
    }
}
