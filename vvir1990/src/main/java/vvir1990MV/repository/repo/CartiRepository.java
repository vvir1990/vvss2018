package vvir1990MV.repository.repo;


import vvir1990MV.model.Carte;
import vvir1990MV.repository.repoMock.CartiRepositoryMock;

import java.io.*;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class CartiRepository extends CartiRepositoryMock {

    private String file = "cartiBD.dat";

    public CartiRepository() {
        URL location = CartiRepository.class.getProtectionDomain().getCodeSource().getLocation();
        System.out.println(location.getFile());

        carti = loadCarti();
    }

    @Override
    public void adaugaCarte(Carte carte) {
        super.adaugaCarte(carte);

        BufferedWriter bw = null;
        try {
            bw = new BufferedWriter(new FileWriter(file, true));
            bw.write(carte.toString());
            bw.newLine();

            bw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private List<Carte> loadCarti() {
        List<Carte> lc = new ArrayList<Carte>();
        BufferedReader br = null;
        try {
            br = new BufferedReader(new FileReader(file));
            String line = null;
            while ((line = br.readLine()) != null) {
                lc.add(Carte.getCarteFromString(line));
            }

            br.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return lc;
    }
}
