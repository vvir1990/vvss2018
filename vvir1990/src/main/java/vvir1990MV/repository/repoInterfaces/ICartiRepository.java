package vvir1990MV.repository.repoInterfaces;


import vvir1990MV.model.Carte;

import java.util.List;

public interface ICartiRepository {
    void adaugaCarte(Carte carte);

    void modificaCarte(Carte nou, Carte vechi);

    void stergeCarte(Carte carte);

    List<Carte> cautaCarte(String ref);

    List<Carte> getCarti();

    List<Carte> getCartiOrdonateDinAnul(Integer an);
}
