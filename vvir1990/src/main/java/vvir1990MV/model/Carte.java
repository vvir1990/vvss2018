package vvir1990MV.model;


import java.util.ArrayList;
import java.util.List;

public class Carte {
    private String isbn;
    private String titlu;
    private List<String> autori;
    private Integer anAparitie;
    private String editura;
    private List<String> cuvinteCheie;

    public Carte() {
        isbn = "";
        titlu = "";
        autori = new ArrayList<String>();
        anAparitie = 0;
        editura = "";
        cuvinteCheie = new ArrayList<String>();
    }

    public String getTitlu() {
        return titlu;
    }

    public void setTitlu(String titlu) {
        this.titlu = titlu;
    }

    public List<String> getAutori() {
        return autori;
    }

    public void setAutori(List<String> autori) {
        this.autori = autori;
    }

    public Integer getAnAparitie() {
        return anAparitie;
    }

    public void setAnAparitie(Integer anAparitie) {
        this.anAparitie = anAparitie;
    }

    public List<String> getCuvinteCheie() {
        return cuvinteCheie;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public String getEditura() {
        return editura;
    }

    public void setEditura(String editura) {
        this.editura = editura;
    }

    public void setCuvinteCheie(List<String> cuvinteCheie) {
        this.cuvinteCheie = cuvinteCheie;
    }

    public void deleteCuvantCheie(String cuvant) {
        for (int i = 0; i < cuvinteCheie.size(); i++) {
            if (cuvinteCheie.get(i).equals(cuvant)) {
                cuvinteCheie.remove(i);
                return;
            }
        }
    }

    public void deleteAutor(String ref) {
        for (int i = 0; i < autori.size(); i++) {
            if (autori.get(i).equals(ref)) {
                autori.remove(i);
                return;
            }
        }
    }

    public void deleteTotiAutorii() {
        autori.clear();
    }

    public void adaugaCuvantCheie(String cuvant) {
        cuvinteCheie.add(cuvant);
    }

    public void adaugaAutor(String autor) {
        autori.add(autor);
    }

    public boolean cautaDupaCuvinteCheie(List<String> cuvinte) {
        for (String c : cuvinteCheie) {
            for (String cuv : cuvinte) {
                if (c.equals(cuv))
                    return true;
            }
        }
        return false;
    }

    public boolean cautaDupaAutor(String autor) {
        for (String a : autori) {
            if (a.contains(autor))
                return true;
        }
        return false;
    }

    public String toString() {
        String autori = "";
        String cuvinteCheie = "";

        for (int i = 0; i < this.autori.size(); i++) {
            if (i == this.autori.size() - 1)
                autori += this.autori.get(i);
            else
                autori += this.autori.get(i) + ",";
        }

        for (int i = 0; i < this.cuvinteCheie.size(); i++) {
            if (i == this.cuvinteCheie.size() - 1)
                cuvinteCheie += this.cuvinteCheie.get(i);
            else
                cuvinteCheie += this.cuvinteCheie.get(i) + ",";
        }

        return isbn + ";" + titlu + ";" + autori + ";" + anAparitie + ";" + editura + ";" + cuvinteCheie;
    }

    public static Carte getCarteFromString(String carteString) {
        Carte carte = new Carte();
        String[] atributeCarte = carteString.split(";");
        String[] autori = atributeCarte[2].split(",");
        String[] cuvinteCheie = atributeCarte[5].split(",");

        carte.isbn = atributeCarte[0];
        carte.titlu = atributeCarte[1];
        for (String s : autori) {
            carte.adaugaAutor(s);
        }
        carte.anAparitie = Integer.parseInt(atributeCarte[3]);
        carte.editura = atributeCarte[4];
        for (String s : cuvinteCheie) {
            carte.adaugaCuvantCheie(s);
        }

        return carte;
    }
}
